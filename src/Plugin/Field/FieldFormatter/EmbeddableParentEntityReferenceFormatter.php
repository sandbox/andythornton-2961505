<?php


namespace Drupal\ef\Plugin\Field\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * When an embeddable is part of a dependent relationship the entity type and
 * entity id is stored on the embeddable as two separate fields. This formatter
 * is associated with the id field, although it just goes back to the entity
 * and grabs the parent entity.
 *
 * @FieldFormatter(
 *   id = "embeddable_parent_entity_reference",
 *   label = @Translation("Rendered entity"),
 *   description = @Translation("Display the parent entity."),
 *   field_types = {
 *     "embeddable_parent_id"
 *   }
 * )
 */
class EmbeddableParentEntityReferenceFormatter extends FormatterBase {
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'view_mode' => 'default',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    // we have to display all view modes, unfortunately
    $all_view_modes = \Drupal::entityQuery('entity_view_mode')
      ->execute();

    $view_modes = [];

    foreach ($all_view_modes as $full_name_view_mode) {
      $vm = substr($full_name_view_mode, strpos($full_name_view_mode, '.') + 1);

      if (!isset($view_modes[$vm])) {
        $view_modes[$vm] = $vm;
      }
    }

    $elements['view_mode'] = [
      '#type' => 'select',
      '#options' => $view_modes,
      '#title' => t('View mode'),
      '#default_value' => $this->getSetting('view_mode'),
      '#required' => TRUE,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $view_mode = $this->getSetting('view_mode');
    $summary[] = t('Rendered as @mode', ['@mode' => $view_mode]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    /** @var \Drupal\ef\EmbeddableInterface $embeddable */
    $embeddable = $items->getEntity();

    // Protect ourselves from recursive rendering.
    static $depth = 0;
    $depth++;
    if ($depth > 20) {
      $this->loggerFactory->get('entity')->error('Recursive rendering detected when rendering embeddable @entity_type @entity_id. Aborting rendering.', array('@entity_type' => $embeddable->bundle(), '@entity_id' => $embeddable->id()));
      return $elements;
    }

    $depth++;

    /** @var \Drupal\Core\Entity\ContentEntityInterface $parent_entity */
    $parent_entity = $embeddable->getParentEntity();

    if ($parent_entity) {
      $view_mode = $this->getSetting('view_mode');
      $entityTypeManager = \Drupal::service('entity_type.manager');
      $view_builder = $entityTypeManager->getViewBuilder($parent_entity->getEntityTypeId());
      $elements = $view_builder->view($parent_entity, $view_mode, $parent_entity->language()->getId());
    }

    return $elements;
  }

}