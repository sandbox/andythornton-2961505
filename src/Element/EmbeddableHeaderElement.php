<?php

namespace Drupal\ef\Element;

use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ef\Decorator\HTMLClassDecoratorFactoryInterface;
use Drupal\ef\EmbeddableViewBuilderInterface;

/**
 * This render element display the title and description used when an
 * embeddable component is used
 *
 * @RenderElement("embeddable_header")
 */
class EmbeddableHeaderElement extends RenderElement {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#pre_render' => [
        [$class, 'preRenderEmbeddableHeaderElement'],
        [$class, 'decorateEmbeddableHeaderElement'],
      ],
      '#title' => NULL,
      '#description' => NULL,
      '#description_format' => 'plain_text',
    ];
  }

  /**
   * Embeddable header prerender callback
   *
   * @param array $element The render array
   * @return array
   */
  public static function preRenderEmbeddableHeaderElement(array $element) {

    if (isset($element['#title'])) {
      $element['#theme_wrappers'] = ['container'];

      $element += [
        'title' => [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $element['#title'],
        ]
      ] ;

      unset($element['#title']);

      if (isset($element['#description'])) {
        $element += [
          'description' => [
            '#type' => 'html_tag',
            '#tag' => 'div',
            '#value' => check_markup($element['#description'], $element['#description_format']),
          ],
        ];

        unset($element['#description']);
      }
    }

    return $element;
  }

  /**
   * Embeddable header prerender callback - decorates with option info
   *
   * @param array $element The render array
   * @return array
   */
  public static function decorateEmbeddableHeaderElement(array $element) {
    if (isset($element['title'])) {
      /** @var  HTMLClassDecoratorFactoryInterface $embeddableDecoratorFactory */
      $embeddableDecoratorFactory = \Drupal::service('ef.html_class_decorator.factory');

      $header_decorator = $embeddableDecoratorFactory->getDecorator('embeddable-header');

      $element['title']['#attributes'] = [
        'class' => [$header_decorator->getClass('title')],
      ];

      if (isset($element['description'])) {
        $element['description']['#attributes'] = [
          'class' => [$header_decorator->getClass('description')],
        ];

      }

      $element['#theme_wrappers'] = [
        'container' => [
          '#attributes' => [
            'class' => [
              $header_decorator->getClass()
            ],
          ]
        ],
      ];
    }

    return $element;
  }
}