<?php

use \Drupal\Core\Entity\Display\EntityFormDisplayInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Hook to allow modules to jump in and add settings to the admin's view mode
 * embeddable framework section.
 *
 * NOTE: When you add in an element to the form you should include a #config
 * element. This should contain the subpath of the config entry that has been
 * defind to store your entries.
 *
 * @see ef_ef_view_mode_settings
 *
 * @param array $form The subset of the form that is the EF option's tab
 * @param EntityFormDisplayInterface $entity_view_display The view display
 * @param string $view_mode The machine name of the view mode
 */
function hook_ef_view_mode_settings (&$form, EntityFormDisplayInterface $entity_view_display, $view_mode) {
  $form['editor_friendly_name'] = [
    '#type' => 'textfield',
    '#title' => t('Editor-friendly view mode name'),
    '#description' => 'By default the embeddable framework provides generic view mode names. Providing an editor-friendly view mode name will help them choose the proper variation when adding embeddables across the site.',
    '#default_value' => $entity_view_display->getThirdPartySetting('ef', 'editor_friendly_name'),
    '#empty_value' => '_none',
    '#weight' => -1,
  ];
}